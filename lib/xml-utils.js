const {xml} = require('@xmpp/component');

function toXML (el) {
	return el.name ? xml(el.name, el.attrs, el.children ? el.children.map(i => toXML(i)) : undefined) : el;
}

module.exports.toXML = toXML;
module.exports.serialize = require("@xmpp/xml/lib/serialize");
