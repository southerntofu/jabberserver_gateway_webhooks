const {xml, jid} = require('@xmpp/component')
const XMPPAdHocCommand = require('~/lib/XMPPAdHocCommand');
const XMPPForm = require('~/lib/XMPPForm');
const xmppError = require('~/lib/xmpp-error');
 
function askConfirmationForm (self, ctx, session) {
	return xml(
		'command', {
			xmlns: 'http://jabber.org/protocol/commands',
			node: self.info.node,
			sessionid: session.id,
			status: 'executing'
		},
		xml(
			'actions',
			{ execute: "complete" },
			xml('complete', {})
		),
		XMPPForm.form({
			title: "Remove project",
			instructions: "WARNING! This will irreversibly remove this project and all its configuration. Subscribers will no longer receive any notifications.",
			fields: {
				confirm: {
					type: "boolean",
					label: "Are you sure?",
					required: true
				}
			}
		})
	);
}

async function removeProject (self, ctx, session) {
	const addressee = ctx.to.bare().toString();
	const sender = ctx.from.bare().toString();
	
	const cmd = ctx.stanza.getChild("command", "http://jabber.org/protocol/commands");
	if (!cmd) {
		return xmppError(ctx.stanza, "bad-request");
	}
	const x = cmd.getChild("x", "jabber:x:data");
	if (x && x.attrs.type == "submit") {
		const form = XMPPForm.submit(x);
		session.data.form = form;
		if (form.confirm) {
			const item = self.parent.parent.items.find(i => i.jid == addressee);
			if (item && item.owner == sender) {
				await self.parent.parent.removeItem(item);
				return xml(
					'command', {
						xmlns: 'http://jabber.org/protocol/commands',
						node: self.info.node,
						sessionid: session.id,
						status: 'completed'
					},
					// TODO Offer "prev" action
					xml(
						'note',
						{ type: "info" },
						`The item ${item.jid} has been removed.`
					)
				);
			} else if (!item) {
				return xmppError(ctx.stanza, "item-not-found");
			} else {
				return xmppError(ctx.stanza, "not-authorized");
			}
		} else {
			return xml(
				'command', {
					xmlns: 'http://jabber.org/protocol/commands',
					node: self.info.node,
					sessionid: session.id,
					status: 'completed'
				},
				// TODO Offer "prev" action
				xml(
					'note',
					{ type: "info" },
					"Command aborted."
				)
			);
		}
	} else {
		return xmppError(ctx.stanza, "bad-request");
	}
}

class WebhookComponentCommandRemove extends XMPPAdHocCommand {
	constructor (settings) {
		if (!settings.node) settings.node = "remove";
		if (!settings.name) settings.name = "Remove this project";
		super(settings);
		
		this.parent = settings.parent;
		
		this.stages = [
			askConfirmationForm,
			removeProject
		];
	}
	
	discoItem (ctx) {
		if (this.parent.parent.registeredUser(ctx)) {
			return super.discoItem(ctx);
		}
	}

}

module.exports = WebhookComponentCommandRemove;

