const {xml, jid} = require('@xmpp/component')
const XMPPAdHocCommand = require('~/lib/XMPPAdHocCommand');
const XMPPForm = require('~/lib/XMPPForm');
const xmppError = require('~/lib/xmpp-error');
 
function addGroupchatForm (self, ctx, session) {
	return xml(
		'command', {
			xmlns: 'http://jabber.org/protocol/commands',
			node: self.info.node,
			sessionid: session.id,
			status: 'executing'
		},
		xml(
			'actions',
			{ execute: "complete" },
			xml('complete', {})
		),
		XMPPForm.form({
			title: "Add groupchat",
			instructions: `Enter the JID of a multiuser chat room to which to send notifications. Please ensure that ${ctx.to} is allowed to participate in the group chat.`,
			fields: {
				groupchatJID: {
					label: "Groupchat JID",
					required: true
				},
				nick: {
					label: "Nickname",
					desc: `The nickname that ${ctx.to} will use in the chatroom`
				}
			}
		})
	);
}

async function addGroupchat (self, ctx, session) {
	const addressee = ctx.to.bare().toString();
	const sender = ctx.from.bare().toString();
	
	const cmd = ctx.stanza.getChild("command", "http://jabber.org/protocol/commands");
	if (!cmd) {
		return xmppError(ctx.stanza, "bad-request");
	}
	const x = cmd.getChild("x", "jabber:x:data");
	if (x && x.attrs.type == "submit") {
		const form = XMPPForm.submit(x);
		if (!form.nick) {
			form.nick = ctx.to.local;
		}
		session.data.form = form;
		if (form.groupchatJID) {
			const item = self.parent.parent.items.find(i => i.jid == addressee);
			if (item && item.owner == sender) {
				const isSubscribed = self.parent.parent.subscriptions.find(s =>
					s.type == "groupchat" &&
					s.from == form.groupchatJID &&
					s.to == addressee &&
					s.nick == form.nick
				);
				if (!isSubscribed) {
					
					await self.parent.parent.addSubscription({
						from: form.groupchatJID,
						to: addressee,
						type: "groupchat",
						nick: form.nick
					});
					
					setTimeout( () => {
						// Make ourselves known to the groupchat
						self.parent.parent.xmpp.send(xml('presence',
							{
								from: addressee,
								to: `${form.groupchatJID}/${form.nick}`,
							},
							xml('nick', {}, form.nick)
						));
					}, 200);
					
					return xml(
						'command', {
							xmlns: 'http://jabber.org/protocol/commands',
							node: self.info.node,
							sessionid: session.id,
							status: 'completed'
						},
						// TODO Offer "prev" action
						xml(
							'note',
							{ type: "info" },
							`${form.groupchatJID} has been subscribed.`
						)
					);
				} else {
					return xmppError(ctx.stanza, "conflict", {text: "Already subscribed"});
				}
			} else if (!item) {
				return xmppError(ctx.stanza, "item-not-found");
			} else {
				return xmppError(ctx.stanza, "not-authorized");
			}
		} else {
			return xml(
				'command', {
					xmlns: 'http://jabber.org/protocol/commands',
					node: self.info.node,
					sessionid: session.id,
					status: 'completed'
				},
				// TODO Offer "prev" action
				xml(
					'note',
					{ type: "info" },
					"Command aborted."
				)
			);
		}
	} else {
		return xmppError(ctx.stanza, "bad-request");
	}
}

class WebhookComponentCommandAddGroupchat extends XMPPAdHocCommand {
	constructor (settings) {
		if (!settings.node) settings.node = "groupchat-add";
		if (!settings.name) settings.name = "Add groupchat subscription";
		super(settings);
		
		this.parent = settings.parent;
		
		this.stages = [
			addGroupchatForm,
			addGroupchat
		];
	}
	
	discoItem (ctx) {
		if (this.parent.parent.registeredUser(ctx)) {
			return super.discoItem(ctx);
		}
	}

}

module.exports = WebhookComponentCommandAddGroupchat;

