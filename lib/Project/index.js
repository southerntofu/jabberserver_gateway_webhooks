const {xml} = require('@xmpp/component')
const commands = require('./commands');
const {toXML} = require('~/lib/xml-utils');

class Project {
	constructor (params = {}) {
		
		this.parent = params.parent;
		this.owner = params.owner;
		this.jid = params.jid;
		this.name = params.name;
		this.url = params.url;
		this.email = params.email;
		this.notifications = params.notifications;
		this.username = params.username;
		this.password = params.password;
		this.allowed = params.allowed;
		this.uuid = params.uuid;
		this.commands = commands.map(c =>
			new c({parent: this, jid: this.jid})
		);
		this.notificationsQueue = (params.notificationsQueue || []).map( i => toXML(i))
	}
	
	discoInfo (ctx) {
		return xml('query', 'http://jabber.org/protocol/disco#info',
			xml(
				'identity',
				{
					category: 'headline',
					type: 'rss',
					name: this.name
				}
			),
			xml(
				'feature',
				{ 'var': 'http://jabber.org/protocol/disco#info' }
			),
			xml(
				'feature',
				{ 'var': 'http://jabber.org/protocol/disco#items', node: "commands" }
			),
			xml(
				'feature',
				{ 'var': 'http://jabber.org/protocol/commands' }
			),
			xml(
				'feature',
				{ 'var': 'urn:xmpp:receipts' }
			),
			xml(
				'feature',
				{ 'var': 'urn:xmpp:chat-markers:0' }
			)
		);
	}
	
	discoItem (ctx) {
		return xml('item', {jid: this.jid, name: this.name});
	}
	
	async flushNotificationsQueue (component) {
		if (!component) return;
		
		for (const stanza of this.notificationsQueue) {
			component.chatMarkers(stanza);
		}
		
		this.notificationsQueue.length = 0;
	}
	
	toJSON () {
		return {
			owner: this.owner,
			jid: this.jid,
			name: this.name,
			url: this.url,
			email: this.email,
			notifications: this.notifications,
			username: this.username,
			password: this.password,
			allowed: this.allowed,
			uuid: this.uuid,
			notificationsQueue: this.notificationsQueue.map(i => i.toJSON ? i.toJSON() : i)
		}
	}
}

module.exports = Project;
