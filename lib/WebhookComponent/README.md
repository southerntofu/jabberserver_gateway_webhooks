# WebhookComponent – An XMPP component to handle webhook notifications

This is a specialisation of [`GenericComponent`](../GenericComponent/README.md).

Its original purpose was to act as a transport to broadcast webhook notifications from code foundries (Gitlab) or generated directly via git hooks. It is generic enough however that it can be used mostly as-is for many other types of webhook integration.

It works by basically specialising the list of items to be instances of [`Project`](../Project/README.md), which have some properties such as the ability to restrict who can subscribe to it (this is controlled by whoever creates the `Project` instance).
