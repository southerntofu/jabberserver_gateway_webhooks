const uuid = require('uuid').v4;
const {xml, jid} = require('@xmpp/component')

const XMPPAdHocCommand = require('~/lib/XMPPAdHocCommand');
const XMPPForm = require('~/lib/XMPPForm');
const xmppError = require('~/lib/xmpp-error');

const Project = require('~/lib/Project');

function sendProjectForm (self, ctx, session) {
	
	if (!session.data.form) {
		session.data.form = {
			allowed: '^.*$'
		};
	}
	
	return xml(
		'command', {
			xmlns: 'http://jabber.org/protocol/commands',
			node: self.info.node,
			sessionid: session.id,
			status: 'executing'
		},
		xml(
			'actions',
			{ execute: "complete" },
			xml('complete', {})
		),
		XMPPForm.form({
			title: "Add new project",
			instructions: `You are about to create an endpoint for receiving webhook notifications. After filling in the required data (most fields are optional) you will be given the endpoint URL to use in your webhooks. This will create an item with a JID of \`handle\`@${self.info.jid}, where \`handle\` is the value that you will enter in the ‘handle’ field below. Authorised users can then subscribe to that JID to receive event updates. You, as the JID owner, will be able to edit its configuration or delete it altogether. Please note that if you unregister from the service ALL ASSOCIATED PROJECTS WILL BE DELETED.`,
			fields: {
				handle: {
					label: "Project handle",
					required: true
				},
				name: {
					label: "Project name",
					required: true
				},
				url: "Homepage",
				email: "Email patches to",
				notifications: {
					type: "boolean",
					label: "Receive chat messages",
					desc: "If enabled, chat messages sent to this JID will be returned by the HTTP API to the caller. It can be used to have a rudimentary form of two-way communications."
				},
// 				username: {
// 					label: "Auth username",
// 					desc: "If you want to protect your webhook endpoint using HTTP Basic Authentication, enter a username here"
// 				},
// 				password: {
// 					label: "Auth password",
// 					desc: "If you want to protect your webhook endpoint using HTTP Basic Authentication, enter a password here"
// 				},
				allowed: {
					label: "Allowed subscribers",
					desc: "Only JIDs matching this regular expression will be allowed to subscribe to this node. Leave the default to allow everyone to subscribe.",
					value: "^.*$"
				}
			}
		})
	);
}

async function createNewProject (self, ctx, session) {
	const sender = ctx.from.bare().toString();
	
	const cmd = ctx.stanza.getChild("command", "http://jabber.org/protocol/commands");
	if (!cmd) {
		return; // FIXME Should return malformed or something
	}
	const x = cmd.getChild("x", "jabber:x:data");
	if (x && x.attrs.type == "submit") {
		const form = XMPPForm.submit(x);
		session.data.form = form;
		if (!form.handle || !form.name) {
			return xml(
				'command', {
					xmlns: 'http://jabber.org/protocol/commands',
					node: self.info.node,
					sessionid: session.id,
					status: 'completed'
				},
				// TODO Offer "prev" action
				xml(
					'note',
					{ type: "error" },
					"A required element is missing. Please provide both a handle and a name."
				)
			);
		}
		
		const projectJID = jid(form.handle, self.parent.jid).toString();
		const existingItem = await self.parent.items.find(i => i.jid == projectJID);
		if (existingItem) {
			if (existingItem.owner != sender) {
				return xml(
					'command', {
						xmlns: 'http://jabber.org/protocol/commands',
						node: self.info.node,
						sessionid: session.id,
						status: 'completed'
					},
					// TODO Offer "prev" action
					xml(
						'note',
						{ type: "error" },
						`The handle ${form.handle} is already in use by someone else. Please try a different value.`
					)
				);
			} else {
				// Updating existing project
			}
		}
		
		form.jid = projectJID;
		form.owner = sender;
		form.uuid = uuid();
		form.parent = self.parent;
		form.notificationsQueue = [];
		const url = (self.parent.settings ? (self.parent.settings.http ? self.parent.settings.http.url : "{id}") : "{id}").replace('{id}', form.uuid);
		const newItem = new Project(form);
		await self.parent.addItem(newItem);
		
		const msg0 = xml(
			'message',
			{
				type: "headline",
				from: projectJID,
				to: sender
			},
			xml('subject', {}, "Your new project has been created"),
			xml('body', {}, `The new project has been created.\nThe webhook endpoint is:\n${url}\n`)
		);
		const msg1 = xml(
			'message',
			{
				type: "headline",
				from: projectJID,
				to: sender
			},
			xml('subject', {}, "Subscription URL for notifications"),
			xml('body', {}, `xmpp:${projectJID}?roster;name=${encodeURIComponent(form.name)}`)
		);
		
		
		setTimeout( ()=> self.parent.xmpp.send([msg0, msg1]), 500);
		
		return xml(
			'command', {
				xmlns: 'http://jabber.org/protocol/commands',
				node: self.info.node,
				sessionid: session.id,
				status: 'completed'
			},
			xml(
				'note',
				{ type: "info" },
				`The new project has been created. The webhook endpoint is: ${url}`
			)
		);
	} else {
		return xmppError(ctx.stanza, "bad-request");
	}
}

class WebhookComponentCommandNew extends XMPPAdHocCommand {
	constructor (settings) {
		if (!settings.node) settings.node = "new";
		if (!settings.name) settings.name = "Add a project";
		super(settings);
		
		this.parent = settings.parent;
		
		this.stages = [
			sendProjectForm,
			createNewProject
		];
	}
	
	discoItem (ctx) {
		if (this.parent.registeredUser(ctx)) {
			return super.discoItem(ctx);
		}
	}

}

module.exports = WebhookComponentCommandNew;

