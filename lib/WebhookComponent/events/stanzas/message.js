const {xml, jid} = require('@xmpp/component')
const xmppError = require('~/lib/xmpp-error');

async function message (stanza) {
	if (stanza.is('message') && stanza.attrs.type == "chat") {
		const sender = jid(stanza.attrs.from).bare().toString();
		const addresse = jid(stanza.attrs.to).bare().toString();
		const item = this.items.find(i => i.jid == addresse);
		if (item && item.notifications) {
			const subscription = this.subscriptions.find(i => i.from == sender && i.to == addresse);
			if (subscription) {
				item.notificationsQueue.push(stanza);
				this.savePersisted();
				this.receipts(stanza);
			}
		}
	}
}

module.exports = message;
