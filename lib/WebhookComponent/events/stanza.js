const handlers = require('./stanzas');

module.exports = async function (stanza) {
	for (const handler of handlers) {
		await handler.call(this, stanza);
	}
	
}
