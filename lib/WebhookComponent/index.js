const {component, xml, jid} = require('@xmpp/component');
const GenericComponent = require('~/lib/GenericComponent');
const commands = require('./commands');
const events = require('./events');

const Project = require('~/lib/Project');

class WebhookComponent extends GenericComponent {
	
	constructor ({settings}) {
		super({settings});
		
		// Component-level commands (replace defaults)
		this.commands = commands.map(c =>
			new c({parent: this, jid: settings.xmpp.domain})
		);
		
		// Component-level events (supplement defaults)
		for (const event in events) {
			if (!Array.isArray(this.events[event])) {
				this.events[event] = [ this.events[event] ];
			}
			this.events[event].push(events[event]);
		}
		
	}
	
	async readPersisted () {
		await super.readPersisted();
		this.items = this.items.map(i => new Project({parent:this, ...i}));
	}
	
	subscriptionAllowed({from, to}) {
		const item = this.items.find(i => i.jid == to);
		if (item) {
			if (item.allowed) {
				const rx = new RegExp(item.allowed);
				return rx.test(from);
			} else {
				// Allow all if item.allowed missing or empty
				return true;
			}
		}
		// Don't allow subscriptions to non-existing projects
		return false;
	}
	
	async sendWebhookNotification (item, message) {
		const stanza = xml(
			'message', {type: (message.type == null || message.type == undefined) ? "headline" : message.type},
			xml('subject', {}, message.subject),
			xml('body', {}, message.body)
		);
		
		await this.sendToItemSubscribers(item, stanza);
	}
	
}

module.exports = WebhookComponent;
