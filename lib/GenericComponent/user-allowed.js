const {jid} = require('@xmpp/component');
 
module.exports = function userAllowed (stanza) {
	const sender = jid(stanza.attrs.from).bare().toString();
	const addressee = jid(stanza.attrs.to).bare().toString();
	
	const settings = this.settings;
	let allowed = false;
	
	if (!settings.registration) {
		return false;
	}
	
	if (settings.registration.deny) {
		if (settings.registration.deny === true) {
			return false;
		}
		for (const denyRule of settings.registration.deny) {
			const denyRx = new RegExp(denyRule, "i");
			if (denyRx.test(sender)) {
				return false;
			}
		}
	}
	
	if (settings.registration.allow) {
		if (settings.registration.allow === true) {
			return true;
		}
		for (const allowRule of settings.registration.allow) {
			const allowRx = new RegExp(allowRule, "i");
			if (allowRx.test(sender)) {
				allowed = true;
			}
		}
	}
	
	return allowed;
}
