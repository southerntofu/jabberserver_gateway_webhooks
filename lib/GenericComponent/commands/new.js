 
const {xml, jid} = require('@xmpp/component')

const XMPPAdHocCommand = require('~/lib/XMPPAdHocCommand');
const XMPPForm = require('~/lib/XMPPForm');
const xmppError = require('~/lib/xmpp-error');


function sendForm (self, ctx, session) {
	
	if (!session.data.form) {
		session.data.form = {
			allowed: '^.*$'
		};
	}
	
	return xml(
		'command', {
			xmlns: 'http://jabber.org/protocol/commands',
			node: self.info.node,
			sessionid: session.id,
			status: 'executing'
		},
		xml(
			'actions',
			{ execute: "complete" },
			xml('complete', {})
		),
		XMPPForm.form({
			title: "Add new item",
			instructions: `This will create an item with a JID of \`handle\`@${self.info.jid}, where \`handle\` is the value that you will enter in the ‘handle’ field below. Authorised users can then subscribe to that JID to receive event updates. You, as the JID owner, will be able to edit its configuration or delete it altogether. Please note that if you unregister from the service ALL ASSOCIATED ITEMS WILL BE DELETED.`,
			fields: {
				handle: {
					label: "handle",
					required: true
				},
				name: {
					label: "name",
					required: true
				}
			}
		})
	);
}

async function createNewItem (self, ctx, session) {
	const sender = ctx.from.bare().toString();
	
	const cmd = ctx.stanza.getChild("command", "http://jabber.org/protocol/commands");
	if (!cmd) {
		return; // FIXME Should return malformed or something
	}
	const x = cmd.getChild("x", "jabber:x:data");
	if (x && x.attrs.type == "submit") {
		const form = XMPPForm.submit(x);
		session.data.form = form;
		if (!form.handle || !form.name) {
			return xml(
				'command', {
					xmlns: 'http://jabber.org/protocol/commands',
					node: self.info.node,
					sessionid: session.id,
					status: 'completed'
				},
				// TODO Offer "prev" action
				xml(
					'note',
					{ type: "error" },
					"A required element is missing. Please provide both a handle and a name."
				)
			);
		}
		
		const itemJID = jid(form.handle, self.parent.jid).toString();
		const existingItem = await self.parent.items.find(i => i.jid == itemJID);
		if (existingItem) {
			if (existingItem.owner != sender) {
				return xml(
					'command', {
						xmlns: 'http://jabber.org/protocol/commands',
						node: self.info.node,
						sessionid: session.id,
						status: 'completed'
					},
					// TODO Offer "prev" action
					xml(
						'note',
						{ type: "error" },
						`The handle ${form.handle} is already in use by someone else. Please try a different value.`
					)
				);
			} else {
				// Updating existing item
			}
		}
		
		form.jid = itemJID;
		form.owner = sender;
		// The item can be anything
		const newItem = Object.assign({createdOn: Date.now()}, form);
		await self.parent.addItem(newItem);
		
		return xml(
			'command', {
				xmlns: 'http://jabber.org/protocol/commands',
				node: self.info.node,
				sessionid: session.id,
				status: 'completed'
			},
			xml(
				'note',
				{ type: "info" },
				"The new item has been created."
			)
		);
	} else {
		return xmppError(ctx.stanza, "bad-request");
	}
}

class ComponentCommandNew extends XMPPAdHocCommand {
	constructor (settings) {
		if (!settings.node) settings.node = "new";
		if (!settings.name) settings.name = "Add an item";
		super(settings);
		
		this.parent = settings.parent;
		
		this.stages = [
			sendForm,
			createNewItem
		];
	}
	
	discoItem (ctx) {
		if (this.parent.registeredUser(ctx)) {
			return super.discoItem(ctx);
		}
	}

}

module.exports = ComponentCommandNew;

