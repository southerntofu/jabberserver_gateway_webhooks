const {xml, jid} = require('@xmpp/component')
const xmppError = require('~/lib/xmpp-error');

async function presenceSubscribe (stanza) {
	if (stanza.is('presence') && stanza.attrs.type == "subscribe") {
		const sender = jid(stanza.attrs.from).bare().toString();
		const addressee = jid(stanza.attrs.to).bare().toString();
		const item = this.items.find(i => i.jid == addressee);
		if (item) {
			if (!this.subscriptionAllowed || await this.subscriptionAllowed({from: sender, to: addressee})) {
				
				if (!this.subscriptions.find(i => i.from == sender && i.to == addressee)) {
					this.addSubscription({from: sender, to: addressee});
				}
				await this.xmpp.send(xml('presence', {
					from: addressee,
					to: sender,
					type: 'subscribed'
				}));
				await this.xmpp.send(xml('presence', {
					from: addressee,
					to: sender
				})); // Send current presence status
				
			} else {
				await this.xmpp.send(xml('presence', {
					from: addressee,
					to: sender,
					type: 'unsubscribed'
				}));
			}
		} else {
			await this.xmpp.send(xmppError(stanza, "item-not-found"));
		}
	}
}

module.exports = presenceSubscribe;
