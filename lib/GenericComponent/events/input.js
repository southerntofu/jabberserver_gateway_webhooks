const logger = require('~/lib/logger');

module.exports = function (input) {
	logger.info('📥', logger.c.blue(input))
}
