const {xml, jid} = require('@xmpp/component')

// Send information about the component itself

module.exports = function (ctx, res) {
	if (ctx.to && !ctx.to.local && this.discoInfo) {
		
		return this.discoInfo(ctx);
		
	}
	
	return res;
}
