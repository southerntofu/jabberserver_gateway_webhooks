const {xml, jid} = require('@xmpp/component')

const handlers = [
	require('./component'),
	require('./item')
];

module.exports = {
	namespace: "http://jabber.org/protocol/disco#info",
	tag: "query",
	handler: async function (ctx) {
		
		let res;
		
		for (const handler of handlers) {
			res = await handler.call(this, ctx, res);
			if (res) {
				break;
			}
		}
		
		return res;
	}
}
