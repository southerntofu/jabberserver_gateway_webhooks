const {xml, jid} = require('@xmpp/component')
const xmppError = require('~/lib/xmpp-error');

// Send information about the component itself

module.exports = function (ctx, res) {
	if (ctx.to && ctx.to.local) {
		
		const sender = ctx.from.bare().toString();
		const addresse = ctx.to.bare().toString();
		const item = this.items.find(p => p.jid == addresse);
		
		if (!item) {
			return xmppError(ctx.stanza, "item-not-found");
		} else if (item.discoInfo) {
			return item.discoInfo(ctx);
		}
		
	}
	
	return res;
}
