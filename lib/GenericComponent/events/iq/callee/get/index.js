const discoInfo = require('./disco-info');

module.exports = [
	require('./disco-info'),
	require('./disco-items'),
	require('./register')
]
