const {xml, jid} = require('@xmpp/component')

const handlers = [
	...require('./component'),
	...require('./item')
];

module.exports = {
	namespace: "http://jabber.org/protocol/disco#items",
	tag: "query",
	handler: async function (ctx) {
		
		let res = xml('query', ctx.element.attrs);
		
		for (const handler of handlers) {
			res = await handler.call(this, ctx, res);
		}
		
		return res;
	}
}
