
// Send list of item children

module.exports = function (ctx, res) {
	if (ctx.to && ctx.to.local && !ctx.element.attrs.node) {
		
		const sender = ctx.from.bare().toString();
		const addresse = ctx.to.bare().toString();
		
		const item = this.items.find(i => i.jid == addresse);
		if (item && item.items) {
			for (const subitem of item.items) {
				if (subitem.discoItem) {
					res.append(subitem.discoItem(ctx));
				}
			}
		}
	}
	
	return res;
}
