
// Send list of item commands

const node = "http://jabber.org/protocol/commands";
module.exports = function (ctx, res) {
	if (ctx.to && ctx.to.local && ctx.element.attrs.node == node) {
		
		const sender = ctx.from.bare().toString();
		const addresse = ctx.to.bare().toString();
		
		const item = this.items.find(i => i.jid == addresse);
		if (item && item.commands) {
			for (const command of item.commands) {
				if (command.discoItem) {
					res.append(command.discoItem(ctx));
				}
			}
		}
		
	}
	
	return res;
}
