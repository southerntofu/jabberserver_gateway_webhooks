const {xml, jid} = require('@xmpp/component')
const xmppError = require('~/lib/xmpp-error');

// Process item commands

module.exports = function (ctx, res) {
	if (ctx.to && ctx.to.local) {
		
		const sender = ctx.from.bare().toString();
		const addresse = ctx.to.bare().toString();
		const commandElement = ctx.stanza.getChild("command", "http://jabber.org/protocol/commands");
		const item = this.items.find(p => p.jid == addresse);
		
		if (item && item.commands) {
			const command = item.commands.find(i => i.info.node == commandElement.attrs.node);
			if (command) {
				return command.despatch(ctx);
			}
		}
		
		return xmppError(ctx.stanza, "item-not-found");
		
	}
	
	return res;
}
