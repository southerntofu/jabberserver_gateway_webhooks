const {xml, jid} = require('@xmpp/component')
const logger = require('~/lib/logger');

module.exports = async function (address) {
	logger.debug('▶', logger.c.green('online as ' + address.toString()))
	
	// Tell everyone we're online
// 	this.presence.set();

	// TODO
	// Deliver messages that might have been added to the incoming
	// database but not yet sent to recipients
	
}
