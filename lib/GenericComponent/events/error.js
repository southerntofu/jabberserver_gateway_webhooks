const logger = require('~/lib/logger');

module.exports = (err) => {
	logger.error('❌', err.toString())
	console.error(err);
}
