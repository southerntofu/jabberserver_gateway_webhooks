const {component, xml, jid} = require('@xmpp/component');
const Datastore = require('nedb-promises');

const events = require('./events');
const iq = require('./events/iq');
const commands = require('./commands');
const userAllowed = require('./user-allowed');

class GenericComponent {
	
	persistedItems = ["registrations", "subscriptions", "items"];
	
	constructor({settings}) {
		this.settings = settings;
		
		this.jid = settings.xmpp.domain;
		this.registrations = [];
		this.subscriptions = [];
		this.items = [];
		
		// Event callbacks
		// Expected structure is {eventName: function (), anotherEventName: function (), …}
		this.events = events;
		
		// Info/query (IQ) callbacks
		// Expected structure is {callee: {get: […], set: […]}}
		this.iq = iq;
		
		// Component-level commands
		this.commands = commands.map(c =>
			new c({parent: this, jid: settings.xmpp.domain})
		);
		
		this.persistence = Datastore.create(settings.persistence ? settings.persistence.path : undefined);
		this.userAllowed = userAllowed.bind(this);
	}
	
	async readPersisted () {
		const store = await this.persistence.findOne({jid: this.jid});
		for (const key of this.persistedItems) {
			if (store && (key in store)) {
				this[key] = store[key];
			}
		}
	}
	
	async savePersisted () {
		const store = this.toJSON();
		await this.persistence.update({jid: this.jid}, store, {upsert:true});
	}
	
	async start () {
		if (!this.xmpp) {
			this.xmpp = component(this.settings.xmpp);
			
			await this.readPersisted();
			
			// Set handlers for iq events
			for (const type of ["get", "set"]) {
				for (const event of this.iq.callee[type]) {
					this.xmpp.iqCallee[type](event.namespace, event.tag, event.handler.bind(this));
				}
			}
			
			// Set handlers for other events
			for (const event in this.events) {
				if (Array.isArray(this.events[event])) {
					for (const handler of this.events[event]) {
						this.xmpp.on(event, handler.bind(this));
					}
				} else {
					this.xmpp.on(event, events[event].bind(this));
				}
			}
			
			return await this.xmpp.start();
		}
	}
	
	async stop () {
		if (this.xmpp) {
			await this.savePersisted();
			// TODO Send 'unavailable' presence
			await this.xmpp.stop();
			delete this.xmpp;
		}
	}
	
	async sendToItemSubscribers (item, stanza) {
		const subscribers = this.subscriptions.filter(s => s.to == item.jid);
		for (const subscriber of subscribers) {
			const payload = stanza.clone();
			payload.attrs.from = item.jid;
			payload.attrs.to = subscriber.from;
			if (subscriber.type == "groupchat") {
				payload.attrs.type = subscriber.type
				const topic = payload.clone();
				topic.remove(topic.getChild("body"));
				await this.xmpp.send(topic);
				payload.remove(payload.getChild("subject"));
			}
			await this.xmpp.send(payload);
		}
	}
	
	async discoInfo (ctx) {
		const res = xml('query', 'http://jabber.org/protocol/disco#info',
			xml(
				'identity',
				{
					category: 'component',
					type: 'generic',
					name: this.settings.xmpp.name
				}
			),
			xml(
				'feature',
				{ 'var': 'http://jabber.org/protocol/disco#info' }
			),
			xml(
				'feature',
				{ 'var': 'http://jabber.org/protocol/disco#items' }
			)
		);
		
		if (this.userAllowed(ctx.stanza)) {
			res.append(
				xml(
					'feature',
					{ 'var': 'jabber:iq:register' }
				)
			);
		}
		
		if (true || this.commands.some(i => i.discoItem && i.discoItem(ctx))) {
			res.append(
				xml(
					'feature',
					{ 'var': 'http://jabber.org/protocol/commands' }
				)
			);
		}
		
		return res;
	}
	
	async register (ctx) {
		if (this.userAllowed(ctx.stanza)) {
			const sender = ctx.from.bare().toString();

			if (ctx.type == "get") {
				const user = this.registrations.find(i => i.jid == sender);
				if (user) {
					return xml(
						'query', 'jabber:iq:register',
						xml('registered'),
						xml('instructions', {},
							"You are already registered to this service."
						)
					);
				} else {
					return xml(
						'query', 'jabber:iq:register',
						xml('instructions', {}, `You are about to register as ${sender}`),
						xml('username')
// 						xml('x', {xmlns: 'jabber:x:data', type: "form"},
// 							xml('field', {type: "boolean", label: "Confirm", "var": "goahead"})
// 						)
					);
				}
			} else if (ctx.type == "set") {
				const index = this.registrations.findIndex(i => i.jid == sender);
				if (ctx.element.getChild('remove')) {
					if (index != -1) {
						this.registrations.splice(index, 1);
						await this.savePersisted();
					}
				} else {
					if (index == -1) {
						this.registrations.push({jid: sender});
						await this.savePersisted();
						setImmediate( () => this.pushCommands(ctx) );
					}
					return xml(
						'query', 'jabber:iq:register',
						xml('instructions', {}, "You are no longer registered")
					);
				}
			}
		}
	}
	
	registeredUser (ctx) {
		const sender = ctx.from.bare().toString();
		return this.registrations.find(i => i.jid == sender);
	}
	
	pushCommands (ctx) {
		this.xmpp.send(xml(
			'message', {from: ctx.to, to: ctx.from},
			xml('subject', "Service commands"),
			xml('query',
				{
					xmlns: 'http://jabber.org/protocol/disco#items',
					node: 'http://jabber.org/protocol/commands'
				},
				...this.commands.map(i => i.discoItem && i.discoItem(ctx))
			)
		))
	}
	
	async addItem (item) {
		this.items.push(item);
		await this.savePersisted();
	}
	
	async removeItem (item) {
		const i = this.items.findIndex(i => i == item);
		if (i != -1) {
			this.items.splice(i, 1);
		}
		await this.savePersisted();
	}
	
	getItem ({jid, uuid}) {
		return this.items.find(i => i.jid == jid || i.uuid == uuid);
	}
	
	async addSubscription (subscription) {
		this.subscriptions.push(subscription);
		await this.savePersisted();
	}
	
	async removeSubscription (subscription) {
		const i = this.subscriptions.findIndex(i => i == subscription);
		if (i != -1) {
			this.subscriptions.splice(i, 1);
		}
		await this.savePersisted();
	}
	
	receipts (stanza) {
		
		const sender = jid(stanza.attrs.from).bare().toString();
		const addressee = jid(stanza.attrs.to).bare().toString();
		const id = stanza.attrs.id;
		const type = stanza.attrs.type || "chat";
		
		const receipt = (stanza ? stanza.getChild("request", "urn:xmpp:receipts") : undefined) &&
			xml("received",
				{
					xmlns: "urn:xmpp:receipts",
					id
				}
			);
			
		const marker = (stanza ? stanza.getChild("markable", "urn:xmpp:chat-markers:0") : undefined) &&
			xml("received",
				{
					xmlns: "urn:xmpp:chat-markers:0",
					id
				}
			);

		if (receipt || marker) {
			
			const ack = xml("message",
				{
					xmlns: "jabber:client",
					from: addressee,
					to: sender,
					type
				},
				receipt,
				marker,
				xml("store", "urn:xmpp:hints")
			);
			
			this.xmpp.send(ack);
			
		}
	}
	
	chatMarkers (stanza) {
		if (stanza ? stanza.getChild("markable", "urn:xmpp:chat-markers:0") : undefined) {
			
			const sender = jid(stanza.attrs.from).bare().toString();
			const addressee = jid(stanza.attrs.to).bare().toString();
			const id = stanza.attrs.id;
			const type = stanza.attrs.type || "chat";
			
			const ack = xml("message",
				{
					xmlns: "jabber:client",
					from: addressee,
					to: sender,
					type
				},
				xml("displayed",
					{
						xmlns: "urn:xmpp:chat-markers:0",
						id
					}
				),
				xml("store", "urn:xmpp:hints")
			);
			this.xmpp.send(ack);
		}
	}
	
	toJSON() {
		const store = {};
		for (const key of this.persistedItems) {
			const item = this[key];
			store[key] = item.toJSON
				? item.toJSON()
				: Array.isArray(item)
					? item.map(i => i.toJSON ? i.toJSON() : i)
					: item;
		}
		store.jid = this.jid;
		return store;
	}
	
}

module.exports = GenericComponent;
