const GitLogParser = require('../../git-log-parser');
 
module.exports = function (req, res, next) {
	if (req.is("text/plain") && req.body) {
		
		const payload = GitLogParser(req.body);
		if (payload.commit && payload.tstamp) {
			res.status(202).end();
			req.app.parent.emit('message', {id: req.params.id, type: "commit", payload: payload});
		}
	}
	next();
}
