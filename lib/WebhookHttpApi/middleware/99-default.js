 
module.exports = function (req, res, next) {
	if (req.body && !res.headersSent) {
		
		res.status(202).end();
		req.app.parent.emit('message', {id: req.params.id, type: req.get("Content-Type"), payload: req.body});
	}
	next();
}
