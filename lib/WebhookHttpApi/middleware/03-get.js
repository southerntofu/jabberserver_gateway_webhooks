const {serialize} = require('~/lib/xml-utils');

function json (req, res, next, item) {
	res.send(item.notificationsQueue);
	item.flushNotificationsQueue(req.app.parent.component);
	req.app.parent.component.savePersisted();
	next();
}


function xml (req, res, next, item) {
	let body = "<?xml version='1.0'?>";
	body += `<stream:stream version="1.0" xmlns="jabber:component:accept" xmlns:stream="http://etherx.jabber.org/streams" to="${req.app.parent.component.jid}">`;
		
	item.notificationsQueue.forEach( notification => {
		body += serialize(notification);
	});
		
	body += "</stream:stream>";
	
	res.send(body);
	item.flushNotificationsQueue(req.app.parent.component);
	req.app.parent.component.savePersisted();
	next();
}

function text (req, res, next, item) {
	const body = item.notificationsQueue.filter(notification => notification.is("message"))
	.map( message => message.getChildText("body") )
	.filter( i => i )
	.join("\n");
	
	res.send(body.length ? (body+"\n") : "");
	item.flushNotificationsQueue(req.app.parent.component);
	req.app.parent.component.savePersisted();
	next();
}

module.exports = function (req, res, next) {
	if (req.method == "GET") {
		const item = req.app.parent.component.getItem({uuid:req.params.id});
		
		if (item? item.notifications : undefined) {
			try {
				const handlers = {
					"application/json": json,
					"application/xml": xml,
					"text/xml": xml,
					"text/plain": text
				};
				
				const mimetype = (handlers[req.query.mime] && req.query.mime) || req.accepts(Object.keys(handlers));
				
				if (mimetype) {
					res.set("Content-Type", mimetype);
					handlers[mimetype](req, res, next, item);
				} else {
					res.status(406).send();
					next();
				}
			} catch (err) {
				next(err);
			}
		} else {
			res.status(404).end();
		}
		
	} else {
		next();
	}
}
