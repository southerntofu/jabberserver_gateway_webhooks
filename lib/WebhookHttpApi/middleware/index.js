
module.exports = [
	require('./01-plain-git'),
	require('./02-gitlab'),
	require('./03-get'),
	require('./99-default')
];
