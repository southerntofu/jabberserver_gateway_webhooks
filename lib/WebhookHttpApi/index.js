
const HttpApi = require('../HttpApi');
const middleware = require('./middleware');

class WebhookHttpApi extends HttpApi {
	
	start () {
		
		const endpoints = {
			"/:id": {
				post: middleware,
				put: middleware,
				get: middleware
			}
		};
		
		super.start(endpoints);
	}
	
}

module.exports = WebhookHttpApi;
