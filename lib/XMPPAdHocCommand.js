const {xml} = require('@xmpp/component')
const xmppError = require('~/lib/xmpp-error');

const defaults = {
	lifetime: 60*3600*1000,
	throttle: 1000, // Minimum interval between purges
	maxSessions: 1000
}

class Sessions {
	constructor (settings = {}) {
		settings = Object.assign({}, defaults, settings);
		
		this.sessions = {};
		this.lastPurge = Date.now();
		this.lifetime = settings.lifetime;
		this.throttle = settings.throw;
		this.maxSessions = settings.maxSessions;
		this.jid = settings.jid;
		this.node = settings.node;
		this.name = settings.name;
	}
	
	get (sessionid) {
		this.purge();
		
		if (!(sessionid in this.sessions)) {
			if (sessionid === undefined) {
				if (Object.keys(this.sessions).length < this.maxSessions) {
					do {
						sessionid = Math.random().toString().substring(2);
					} while (sessionid in this.sessions);
				} else {
					return;
				}
			}
			
			this.sessions[sessionid] = {
				id: sessionid,
				tstamp: Date.now(),
				stage: 0,
				data: {}
			}
		}
		
		return this.sessions[sessionid];
	}
	
	save (session) {
		if ("id" in session) {
			this.sessions[session.id] = session;
		}
	}
	
	destroy (session) {
		delete this.sessions[session.id];
	}
	
	purge () {
		const now = Date.now();
		if ((now - this.lastPurge) > this.throttle) {
			for (const id in this.sessions) {
				const age = now - this.sessions[id].tstamp;
				if (age > this.lifetime) {
					this.destroy(session);
				}
			}
			this.lastPurge = now;
		}
	}
}
 
class XMPPAdHocCommand {
	constructor ({parent, jid, node, name}, stages = []) {
		this.parent = parent;
		this.stages = stages;
		this.info = {jid, node, name};
		this.sessions = new Sessions();
	}
	
	discoItem () {
		return xml('item', this.info);
	}
	
	async despatch (ctx) {
		const session = this.sessions.get(ctx.element.attrs.sessionid);
		const action = ctx.element.attrs.action || "next"
		
		if(!session) {
			const msg = "Too many commands running at the same time. Try later";
			return xmppError(ctx.stanza, "resource-constraint", msg);
		}
		
		switch (action) {
			case "next":
			case "execute":
				return await this.next(ctx, session);
			case "prev":
				return await this.prev(ctx, session);
			case "cancel":
				return await this.cancel(ctx, session);
			case "complete":
				return await this.last(ctx, session);
			default:
				// Error?
		}
	}
	
	async next (ctx, session) {
		const stage = this.stages[session.stage];
		
		if (stage) {
			const response = await stage(this, ctx, session);
			session.stage++;
			this.sessions.save(session);
			return response;
		}
	}
	
	async prev (ctx, session) {
		session.stage--;
		return await this.next(ctx, session);
	}
	
	async last (ctx, session) {
		session.stage = this.stages.length-1;
		return await this.next(ctx, session);
	}
	
	async cancel (ctx, session) {
		this.sessions.destroy(session);
		
		const attrs = Object.assign({}, ctx.element.attrs, {status: "canceled"});
		return xml('command', attrs);
	}
}

module.exports = XMPPAdHocCommand;
