const fs = require('fs');
const path = require('path');

module.exports = function (dir) {
	const files = fs.readdirSync(dir, {withFileTypes: true}) // Only take…
		.filter(f => f.isFile() && // …files…
			path.extname(f.name) == '.js' && // …with the .js extension…
			f.name[0] != '.' && // …which are not hidden…
			f.name != 'index.js' // …and exclude index.js.
		);

	const list = {};
	for (const file of files) {
		list[path.basename(file.name, '.js')] = require(`${dir}/${file.name}`);
	}
	return list;
}
