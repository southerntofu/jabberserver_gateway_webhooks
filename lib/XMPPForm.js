const {xml} = require('@xmpp/component')

function isMultivalue(fieldType) {
	return ["list-multi", "jid-multi", "text-multi"].includes(fieldType);
}

function form ({title, instructions, fields = {}, values = {}}) {
	const result = xml('x', {xmlns: 'jabber:x:data', type: 'form'});
	
	if (title) {
		result.append(xml('title', {}, title));
	}
	
	if (instructions) {
		result.append(xml('instructions', {}, instructions));
	}
	
	for (const name in fields) {
		
		const field = typeof fields[name] === 'object'
			? fields[name]
			: { label: fields[name] };
			
		const entity = xml(
			'field',
			{
				type: field.type,
				label: field.label,
				"var": name
			}
		);
		
		if (field.required) {
			entity.append(xml('required'));
		}
		
		if (field.desc) {
			entity.append(xml('desc', {}, field.desc));
		}
		
		if (field.options) {
			for (const option of field.options) {
				if (typeof option === 'object') {
					entity.append(
						xml('option', {label: option.label}, xml('value', {}, option.value))
					);
				} else {
					entity.append(xml('option', {label: option}, xml('value', {}, option)));
				}
			}
		}
		
		const value = (values[name] == null || values[name] == undefined) ? field.value : values[name];
		
		if (value) {
			const list = Array.isArray(value) ? value : [value];
			if (!isMultivalue(field.type)) {
				list.length = 1;
			}
			for (const item of list) {
				entity.append(xml('value', {}, item));
			}
		}
		
		result.append(entity);
	}
	
	return result;
}


function submit (stanza) {
	const result = {};
	const fields = stanza.getChildren('field');
	for (const field of fields) {
		const name = field.attrs.var;
		const type = field.attrs.type;
		const values = field.getChildren('value');
		result[name] = values.map(value => value.text()).filter(text => text.length);
		if (result[name].length == 1 || !isMultivalue(type)) {
			result[name] = result[name][0];
		}
		if (type == "boolean") {
			if (result[name] == "1" || result[name] == "true") {
				result[name] = true;
			} else {
				result[name] = false;
			}
		}
	}
	return result;
}

module.exports = { form, submit };
