# Webhook XMPP Component – receive notifications on webhook events

This component lets authorised users create “projects” which provide a webhook endpoint to receive notifications on. Other XMPP users can subscribe to the project JID and then receive notifications on webhook events.

Its original purpose was to receive notifications from Git, Git foundries and so on, but it can be easily extended for use in many other contexts – see the sections on ‘other integrations’ and ‘templates’ for more information on this.

## Installation

Clone this repository, then:

```bash
cd webhooks
npm install
```

## Configuration

Site-specific configuration is specified in two files in the project's top directory: `settings.yaml` and `templates.yaml`.

Example `settings.yaml`:

```yaml
---
xmpp:
    # Service name, used in service discovery
    name: Git Notifications
    # Host and port in which to reach the XMPP server.
    # Must match the configuration on the server.
    service: xmpp://example.net:5231
    # Domain to make ourselves available as. Must be
    # compatible with the XMPP server configuration
    # and should have a TLS certificate.
    domain: git.example.net
    # Password used by the component to identify itself
    # to the server. Must match the server configuration.
    password: xxxxxx
    
persistence:
    # Where to persist the data between restarts (optional but recommended).
    path: ./data.nedb
        
registration:
    # Who can / cannot create “projects” in this instance. A list
    # of regular expressions.
    allow:
        - "^.*@example.net$"
    deny: []

http:
    # Details of the webhook server. Typically this should
    # run behind a proxy such as Nginx.
    host: "127.0.0.1"
    port: 3001
    path: /webhooks
    # This is a template of the *public* URL of the webhook endpoints.
    # The `{id}` parameter will be replaced with the random
    # UUID corresponding to each project. The final URL is shown to the
    # user on project creation.
    url: "https://example.net/webhooks/{id}"
```

## Usage

When started and connected to the XMPP server, a new item will be available on the server upon running a service discovery query (therefore, a client supporting service discovery should be used). A discovery query might return, for instance:

```
example.net
│ 
├ conference.example.net
├ upload.example.net
└ git.example.net
```

Querying `git.example.net`, when done by users matching the `registration.allow` property and not matching `registration.deny` in `settings.yaml` will reveal an `Add a project` command. When executed, a form will be shown where the user can configure the details of his ‘project’. A project has both an XMPP JID (chosen by the user), to which other people may subscribe, and an HTTP API implementing the webhook. The URL of this API is a combination of the parameters in `settings.yaml` and a random identifier, therefore not under control of the user creating the project.

After a project has been created, it is revealed in the service discovery results:

```
example.net
│ 
├ conference.example.net
├ upload.example.net
└ git.example.net
  ├ recipeapp@git.example.net
  └ myproject@git.example.net
```

Any authorised user can subscribe to any of the available projects and will receive notifications on webhook events. Additionally, via an ad hoc command, the project owner can add chatroom subscriptions so that the component items can participate in project MUC rooms.

## Notifications

### Recognised webhook events

#### Plain Git

Save this as `./.git/hooks/post-commit` in your Git project directory:

```bash
#!/bin/bash

# This is your webhook endpoint, obtained when creating the project item
# in the XMPP server via the `Add a project` command.
URL="https://example.net/webhooks/c83c53f0-4e09-4aea-8ecc-79077594bc4f"

git log -n 1 --format=medium |
        curl -qs "$URL" -H "Content-Type: text/plain" --data-binary @-

exit 0

```

Every time you commit something in your computer (and assuming you have connectivity to the webhook endpoint) a notification will be sent to all subscribers to the project JID.

#### Gitlab

In the project settings in Gitlab, add the webhook URL and enable the event types that you are interested in (only `push`, `issue` and `note` events have templates defined at this time, but you can easily add appropriate entries to `templates.yaml` – see below for details).

#### Other integrations

Write a handler in `GitHttpApi/middleware` and add it to `GitHttpApi/middleware/index.js`. Your handler should call `next()` without taking any actions if it doesn't recognise the payload. If it does, it should call `req.app.parent.emit('message', {id: req.params.id, type: "some_event_type", payload: req.body})` or something similar and return [HTTP 202 Accepted](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/202). Look at the existing handlers for guidance.

### Notification templates

When a `message` event is received, the component looks at `templates.yaml` for an [EJS](https://ejs.co/) template to use to prepare the actual XMPP message. The keys in `templates.yaml` correspond to the `type` parameter in the `message` payload. For instance, a plain Git commit will emit a `message` like:

```json
{
  "id": "c83c53f0-4e09-4aea-8ecc-79077594bc4f",
  "type": "commit",
  "payload": {
    "subject": "First line of commit message",
    "message": "First line of commit message\n\nDetailed commit description.\n",
    "author": "A. Commiter <commiter@example.net>",
    "tstamp": "1970-01-01T00:00:00.000Z",
    "commit": "20436fb7dcd66de4f7a12436c640f03129bbdf18"
  }
}
```

This will match the `commit` key in `templates.yaml`.

Each template consists of a `subject` part and a `body` part, as notifications are sent by default with a type of `headline`. For chat room notifications, the subject is sent as a separate message which will cause the room subject to be changed, if allowed by the latter's configuration settings.

The webhook notification payload is available to the template as the `data` property. You are free to format the message in whichever way it suits your users' needs.

## Security

> This is not production quality software. It was put together from various bits of internal code copied and pasted together in a haphazard way. Users should do their own risk assessment before deploying this code.

### Confidentiality

End users should be aware that the component administrator (and the XMPP server administrator, if different) have access to the list of defined ‘projects’, their properties and the list of subscribers, all of which information may be persisted on disk. The component (and if different, XMPP and HTTP proxy) server administrator(s) also have access to the webhook payloads. Users should only create projects in servers that they trust sufficiently for this purpose.

Subscribers to a project JID should know that their JID is accessible to the component administrator and that this information is persisted to disk. The component does not request nor require presence information from subscribers, but some clients pre-emptively send subscription authorisation and presence information to JIDs they subscribe to – meaning that the component administrator may be able to see subscribers' presence status.

The connection between the component and the XMPP server is not encrypted. Risks derived from this may be mitigated by running the component on the same host as the XMPP server or encapsulating the connection in an encrypted tunnel.

The HTTP API is not encrypted. It is strongly recommended to have the HTTP API listening on the loopback interface only and to proxy the connection via a properly secured HTTPS server, such as Nginx.

The list of projects, their properties and subscribers may be saved to disk if so configured by the component administrator (and this will usually be the case). The disk file is unencrypted. Administrators concerned by this risk should ensure that the file resides in an encrypted filesystem and that access to it is adequately restricted.

The component does not support end to end encryption. This is by design as it would not mitigate any of the risks mentioned above.

### Integrity

The webhook handlers provided in this repository as of this writing do not support any form of integrity checking. Where available (e.g., [Gitea webhooks](https://docs.gitea.io/en-us/webhooks/) appear to provide an HMAC hash), it is the responsibility of the handler author to make their own decisions as to integrity assurance.

### Availability

The component supports persisting project properties and subscriptions to disk but this is an optional feature. If not used, project definitions will be lost when the component shuts down.

A loss of connectivity between the component and the XMPP server will cause it to be unavailable. Any messages received while disconnected may be irretrievably lost. The component will try to reconnect to the server at regular intervals.

A loss of connectivity between the component and the HTTP proxy, if one is used, will cause any webhook notifications to not be received – this will normally be detectable by the sender. What happens in this case depends on the sending agent, which may or may not try to resend the notification.

If the subscriber is offline, messages will typically be held by the server until the user becomes available again. Whether messages are then archived by the server or by the client depends on their respective configurations. The component sends messages as type `headline`, with no explicit storage instructions.

In order to be able to create and manage ‘projects’, users must have an XMPP client providing a service discovery ([XEP-0030: Service Discovery](https://xmpp.org/extensions/xep-0030.html)) interface and supporting [XEP-0050: Ad-Hoc Commands](https://xmpp.org/extensions/xep-0050.html) and [XEP-0004: Data Forms](https://xmpp.org/extensions/xep-0004.html).

Users merely wishing to receive notifications only need to be able to discover a project's JID somehow, either via a service discovery mechanism or by virtue of the JID being announced publicly, e.g., in the project's documentation.

Participants in chatrooms which have been signed up to receive notifications by a project's creator do not need to perform any form of discovery.

### Authenticity

The component does not support, at present, any form of authentication on the webhook endpoints.

Authentication of XMPP users is the responsibility of the XMPP server hosting their accounts.
