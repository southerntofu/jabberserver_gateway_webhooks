#!/usr/bin/node

const fs = require('fs');
const path = require('path');
const YAML = require('yaml');
const ejs = require('ejs');

process.env.NODE_PATH = __dirname;

const logger = require('./lib/logger');
const WebhookHttpApi = require('./lib/WebhookHttpApi');
const WebhookComponent = require('./lib/WebhookComponent');

logger.log(logger.main, '⏼', 'starting up');

// Read configuration
const settings = YAML.parse(fs.readFileSync(path.join(__dirname, "settings.yaml")).toString("utf8"));
logger.log(logger.main, '📖', 'read configuration');

const templates = YAML.parse(fs.readFileSync(path.join(__dirname, "templates.yaml")).toString("utf8"));
logger.log(logger.main, '📖', 'read templates');

const api = new WebhookHttpApi(settings.http);
logger.log(logger.main, '🔖', 'HTTP API created');

const component = new WebhookComponent({settings});
logger.log(logger.main, '🔖', 'XMPP component created');

api.component = component;
api.on('message', async (data) => {
	console.log("MESSAGE", data);
	const tpl = templates ? templates[data.type] : undefined;
	if (tpl) {
		const project = component.items.find(i => i.uuid == data.id);
		if (project) {
			const subject = ejs.render(tpl ? tpl.subject : "", {data: data.payload, project});
			const body = ejs.render(tpl ? tpl.body : "", {data: data.payload, project});
			
			console.log("NOTIFICATION", project.toJSON(), {subject, body});
			await component.sendWebhookNotification(project, {subject, body});
		}
	}
});



async function start () {
	
	await component.start();
	logger.log(logger.main, '⏯', 'component initialised');
	
	api.start();
	logger.log(logger.main, '⏯', 'API initialised');
	
	logger.info(logger.main, '⏻', 'system started');
}

process.on('SIGINT', async () => {
	const t = setTimeout(() => {
		logger.warn("⚠", logger.c.red("Forcing exit!"));
		process.exit();
	}, 5000);
	
	logger.log(logger.main, '⏾', 'shutting down API');
	await api.stop();
	
	logger.log(logger.main, '⏾', 'shutting down component');
	await component.stop();
	
	clearTimeout(t);
	logger.info(logger.main, '⏾', 'shut down complete');
});

process.on('SIGUSR1', async () => {
	logger.debug(logger.main, 'SIGUSR1 received. Dumping state');
	console.log(JSON.stringify(component, null, 4));
});

process.on('SIGUSR2', async () => {
	logger.debug(logger.main, 'SIGUSR2 received. Saving state');
	await component.savePersisted();
});

start();
